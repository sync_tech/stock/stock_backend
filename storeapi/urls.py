from django.urls import path, include
# from . import views
from rest_framework import routers
from .views import ProductViewSet, ProductSearch, ExpenseViewSet, ExpenseSearch, SaleListView, SaleViewSet, \
    SaleDetailView , NotificationListView , SaleReportListView, ChangePassword
from rest_framework.authtoken.views import obtain_auth_token 


router = routers.DefaultRouter()
# router.register('product',views.storeapiView)

router.register('expenses', ExpenseViewSet)
router.register('products', ProductViewSet)
router.register('sales', SaleViewSet)
# router.register('sales', SaleListView.as_view(), basename="me")
# router.register('sales', SaleViewSet)
# router.register('sales',Sales, basename='MyModel')


urlpatterns = [
    # path(r'product')
    path('', include(router.urls)),
    path('token', obtain_auth_token, name="obtain_token_auth"),
    path('change_password/', ChangePassword.as_view() ),
    path('expense/', ExpenseSearch.as_view()),
    path('product/', ProductSearch.as_view()),
    path('salesproduct/', SaleListView.as_view()),
    path('salesproduct/<uuid:pk>', SaleDetailView.as_view()),
    path('notifications/',NotificationListView.as_view()),
    path('salereport/',SaleReportListView.as_view()),

]


