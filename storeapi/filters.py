import django_filters
from django.db import models as django_models
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from .models import Product, Expense, Sale





class ProductFilter(FilterSet):

    class Meta:
        model = Product
        fields = {
            'created_at': ['exact','lte', 'gte', 'lt', 'gt'],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }
        # fields = {
        #     'started__lte': ['fecha__inicio__lte'],
        #     'started__gte': ['fecha__inicio__gte'],
        # }



class ExpenseFilter(FilterSet):
    
    class Meta:
        model = Expense
        fields = {
            'created_at': ['exact','lte', 'gte', 'lt', 'gt'],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }



class SaleFilter(FilterSet):
    
    class Meta:
        model = Sale
        fields = {
            'created_at': ['exact','lte', 'gte', 'lt', 'gt'],
        }

        filter_overrides = {
            django_models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }