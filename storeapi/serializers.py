from rest_framework import serializers
from .models import Product, Sale , Expense


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
    

    # def validate_name(self,value):
    #     pd = Product.objects.filter(name__iexact = value)
    #     if self.instance:
    #         pd.exclude(pk = self.instance.pk)
    #     if pd.exist():
    #         raise serializers.ValidationError("This Product has been used")
    #     return value




class SaleSerializer(serializers.ModelSerializer):
    # product = ProductSerializer(many=False, read_only=True)

    class Meta:
        model = Sale
        # depth = 1
        fields = '__all__'
        # exclude = []


    # def create(self, request, *args, **kwargs):
        # dota = request.data
        # print("Req")
        # print(request)
        # return dota

class SaleReadSerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=False, read_only=True)
    class Meta:
        model = Sale
        fields = '__all__'
    

# class SaleWriteSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Sale
#         fields = '__all__'

class ExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        fields = '__all__'


# class SaleSerializer(serializers.ModelSerializer):
#     product = ProductSerializer(read_only=True, many=False)
#     # test = serializers.RelatedField(many=True)
#     class Meta:
#         model = Sale
#         fields = '__all__'
#         # fields = ['id','sale_product','quantity','total_price','created_at','updated_at']


#     def create(self, validated_data):
#         products_data = validated_data.pop('product')
#         print("olla \n")
#         print(products_data)
#         print("olla end \n")
#         # musician = Musician.objects.create(**validated_data)
#         # for album_data in albums_data:
#             # Album.objects.create(artist=musician, **album_data)
#         # return musician


# https://django.cowhite.com/blog/create-and-update-django-rest-framework-nested-serializers/