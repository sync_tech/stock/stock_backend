# Generated by Django 3.0.4 on 2020-05-04 10:26

from decimal import Decimal
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('storeapi', '0004_auto_20200504_0754'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='sale_price',
            field=models.DecimalField(decimal_places=2, default=1, max_digits=20, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))]),
            preserve_default=False,
        ),
    ]
