from django.contrib import admin
from .models import Product, Sale, Expense
# , Cart


admin.site.register(Product)
admin.site.register(Sale)
admin.site.register(Expense)
# admin.site.register(Cart)
# Register your models here.
