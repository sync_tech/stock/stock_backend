from __future__ import unicode_literals
import uuid
from django.db import models
from django.core.validators import MinValueValidator
from decimal import Decimal
# Create your models here.
"""
 TODO
 fix null and not null fields
 fix empty and not empty  fields

"""


class Product(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50, unique=True)
    quantity = models.PositiveIntegerField(null=True, blank=True, default=1)
    cost_price = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    sale_price = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    shelf_address = models.CharField(max_length=50, null=True,blank=True)
    creatorUser = models.CharField(max_length=80, null=True, blank=True, default="Default User")
    description = models.TextField(null=True, blank=True, default='Description will be here...')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # expiredate = models.DateTimeField()
    # purchased price
    # sales price 

    def __str__(self):
        return self.name
    
    def __unicode__(self):
        return self.name

# class Sale(models.Model):
#     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
#     # name = models.CharField(max_length=50,)
#     # quantity = models.IntegerField(default=1)
#     takenBy = models.CharField(max_length=50)
#     discount = models.DecimalField(max_digits=20, decimal_places=2)
#     saleBy = models.CharField(max_length=50)
#     products = models.ManyToManyField(Product, through='Cart')
#     reason = models.TextField(default='Reason will be here...')
#     # products = models.ManyToManyField(Product,on_delete=models.PROTECT)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)

#     def __str__(self):
#         return self.saleBy



class Sale(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name='sale_product')
    quantity = models.IntegerField(default=1)
    cost_price = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))],default=1)
    sale_price = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))], default=1)
    total_price = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # def __str__(self):
    #     return self.total_price

93 / 30

class Expense(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    reason = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.reason


# class Cart(models.Model):
#     sale = models.ForeignKey(Sale,on_delete=models.CASCADE)
#     product = models.ForeignKey(Product, on_delete=models.CASCADE)
#     quantity = models.IntegerField(default=1)