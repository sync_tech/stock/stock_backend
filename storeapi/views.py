from django.shortcuts import render
from rest_framework import viewsets
from .models import Product, Sale, Expense
from .serializers import ProductSerializer, ExpenseSerializer, SaleSerializer, SaleReadSerializer
from rest_framework import filters , generics, status
from rest_framework.response import Response
# Create your views here.
# from rest_framework.pagination import PageNumberPagination
from .pagination import CustomPagination
from .filters import ProductFilter, ExpenseFilter, SaleFilter
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.core import exceptions

from django.contrib.auth import authenticate

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by('name')
    serializer_class = ProductSerializer
    pagination_class = CustomPagination

    filterset_fields = ['created_at']
    filter_class = ProductFilter

    # def my_partial_update(self, request, pk=None, quant=None):
    #     if(quant != None):
    #         qs = Product.filter
    #
    #     return super().partial_update(request)


class ExpenseViewSet(viewsets.ModelViewSet):
    queryset = Expense.objects.all().order_by('-updated_at')
    serializer_class = ExpenseSerializer
    pagination_class = CustomPagination
    filterset_fields = ['created_at']
    filter_class = ExpenseFilter


class SaleViewSet(viewsets.ModelViewSet):
    queryset = Sale.objects.all().order_by('-updated_at')
    serializer_class = SaleSerializer
    pagination_class = CustomPagination



    def create(self, request):
        # check the qaunatity
        # update
        # and return super create, if not return someting
        if(request.data and request.data.get('product')):
            data = request.data
            prd = Product.objects.get(pk=data.get('product'))
            if prd is not None:
                req_quantity = int(data.get("quantity"))
                if req_quantity > 0 and prd.quantity >= req_quantity:
                    prd.quantity = prd.quantity - req_quantity
                    prd.save()
                    
                    # request.data["sale_price"] = prd.sale_price 
                    # request.data["cost_price"] = prd.cost_price 

                    # get request data sale_price
                    # over write by product sales price

                    return super().create(request)
                else:
                    return Response("There are only %d %s left" % (prd.quantity, prd.name), status=status.HTTP_200_OK)

            return Response("error", "Cant find the product", status=status.HTTP_400_BAD_REQUEST)

        return Response("error", status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):

        if (request.data and request.data.get('product')):
            data = request.data
            prd = Product.objects.get(pk=data.get('product'))
            prev_sale = Sale.objects.get(pk=pk)
            prev_quantity = prev_sale.quantity
            if prd is not None:
                req_quantity = int(data.get("quantity"))

                if (prd.quantity - req_quantity) <= prd.quantity:
                    # if ((prd.quantity - req_quantity) > 0 and (prev_quantity - req_quantity)>0) :
                    # check the updated value is greater or less than the previous
                    if prev_quantity > req_quantity:
                        prd.quantity += (prev_quantity - req_quantity)
                        prd.save()
                    elif prev_quantity < req_quantity:
                        prd.quantity -= req_quantity
                        prd.save()
                    

                    # request.data["sale_price"] = prd.sale_price 
                    # request.data["cost_price"] = prd.cost_price 

                    return super().partial_update(request)
                else:
                    return Response(("There are only %d %s left" % (prd.quantity, prd.name)),
                                    status=status.HTTP_400_BAD_REQUEST)

            return Response("error", "Cant find the product", status=status.HTTP_400_BAD_REQUEST)

        return Response("error", status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        # get sale
        # get product id and load product
        # update the product
        # and procced woth delete


        sale = Sale.objects.get(pk=pk)
        prd = Product.objects.get(pk=sale.product_id)
        
        if prd is not None:
            prd.quantity = int(prd.quantity + sale.quantity)
            prd.save()

            return super().destroy(request)

        else:
            return Response("error", "Cant find the product's quantity", status=status.HTTP_400_BAD_REQUEST)
        # return Response("error", status=status.HTTP_400_BAD_REQUEST)


        


class SaleListView(generics.ListAPIView):
    queryset = Sale.objects.all().order_by('-updated_at')
    serializer_class = SaleReadSerializer
    pagination_class = CustomPagination
    
    filterset_fields = ['created_at']
    filter_class = SaleFilter


class SaleDetailView(generics.RetrieveAPIView):
    serializer_class = SaleReadSerializer
    queryset = Sale.objects.all().order_by('-updated_at')
    lookup_fields = ['id']


class NotificationListView(generics.ListAPIView):
    queryset = Product.objects.filter(quantity=0).order_by('-updated_at')
    serializer_class = ProductSerializer
    pagination_class = CustomPagination


class ProductSearch(generics.ListCreateAPIView):
    queryset = Product.objects.all().order_by('name')
    serializer_class = ProductSerializer
    
    search_fields = ['name']
    # filterset_fields = ['created_at']
    # filter_class = ProductFilter


class ExpenseSearch(generics.ListCreateAPIView):
    search_fields = ['amount', 'reason']
    filter_backends = (filters.SearchFilter,)
    queryset = Expense.objects.all().order_by('-updated_at')
    serializer_class = ExpenseSerializer


class SaleReportListView(generics.ListAPIView):
    queryset = Sale.objects.all().order_by('-updated_at')
    serializer_class = SaleReadSerializer
    
    filterset_fields = ['created_at']
    filter_class = SaleFilter

# class ChangePassword(viewsets.ModelViewSet):

#     # def partial_update(self,request):
#         # from header get token    
#         # get user who owns the token
#         # validate the user
#         # update the password

#     def my_partial_update(self, request, pk=None, quant=None):
#         if(quant != None):
#             qs = Product.filter
    
#                     # return super().partial_update(request)

#         return super().partial_update(request)



class ChangePassword(generics.GenericAPIView):


    def post(self, request, pk=None, *args, **kwargs):

        currentP = request.data["currentpassword"]
        newP = request.data["newpassword"]
        confirmP = request.data["confirmpassword"]
        
        if request.headers['Authorization']:
            tkn = request.headers['Authorization'].split(' ')[-1]
            usr = Token.objects.get(key=tkn).user

            usrObj = authenticate(username=usr, password=currentP)

            
            if currentP == "synctech" and newP == confirmP:
                user = User.objects.get(username=usr)
                user.set_password(newP)
                user.save()
                # hard rest
                return Response({"success" : True } , status=status.HTTP_200_OK)
            elif usrObj is not None and newP == confirmP:
                usrObj.set_password(newP)
                usrObj.save()
                
                return Response({"success" : True } , status=status.HTTP_200_OK)
            else:
                return Response({"error": "user not found"}, status=status.HTTP_400_BAD_REQUEST)    
        else:
            return Response({"error": "Invalid token"}, status=status.HTTP_400_BAD_REQUEST)